﻿using System.Collections.Generic;
using Assets.Classes.Orders;
using Assets.Scripts;
using UnityEngine;

namespace Assets.Classes.Ai
{
    public class DefendStrategy : AiStrategy 
    {
        public DefendStrategy(AiController aiController) : base(aiController)
        {

        }

        public override void Update()
        {
            if (!(Time.time > NextOrdersCheck))
                return;

            NextOrdersCheck += AiController.NewOrdersFrequency;

            var objectivesToDefend = new List<GameObject>(ObjectiveManager.EnemyObjectives);
            objectivesToDefend.Add(AiController.AiBase);

            foreach (var unit in AiController.Units)
            {
                var controller = unit.GetComponent<UnitController>();

                var order = RunChecks(unit, Config.AiFallBackHitpointsPercantage, 0.5f);
                if (order != null)
                {
                    controller.UnitOrder = order;
                    controller.UnitOrder.Execute();
                    continue;
                }

                if (controller.UnitOrder == null)
                {
                    var movePoint = objectivesToDefend[Random.Range(0, objectivesToDefend.Count - 1)].transform.position;
                    order = new MoveOrder(unit, movePoint);
                } else continue;

                controller.UnitOrder = order;
                controller.UnitOrder.Execute();

            }
        }
    }
}
