﻿using System.Collections.Generic;
using System.Linq;
using Assets.Classes.Orders;
using Assets.Scripts;
using UnityEngine;

namespace Assets.Classes.Ai
{
    public class CaptureStrategy : AiStrategy
    {
        private GameObject _aiBase;
        private List<GameObject> _objectivesBeeingCaptured;

        public CaptureStrategy(AiController aiController) : base(aiController)
        {
            _aiBase = GameObject.Find("EBase");
            _objectivesBeeingCaptured = new List<GameObject>();
        }

        public override void Update()
        {
            if (!(Time.time > NextOrdersCheck))
                return;

            NextOrdersCheck += AiController.NewOrdersFrequency;

            var objectivesToCapture = ObjectiveManager.NeutralObjectives.Count > 0 
                ? ObjectiveManager.NeutralObjectives 
                : ObjectiveManager.PlayerObjectives;
            objectivesToCapture.RemoveAll(x => _objectivesBeeingCaptured.Contains(x));
            objectivesToCapture = objectivesToCapture.OrderBy(x => Vector3.Distance(x.transform.position, _aiBase.transform.position)).ToList();
            var index = 0;

            foreach (var unit in AiController.Units)
            {
                var controller = unit.GetComponent<UnitController>();

                var order = RunChecks(unit, Config.AiFallBackHitpointsPercantage, 2.5f);
                if (order != null)
                {
                    controller.UnitOrder = order;
                    controller.UnitOrder.Execute();
                    continue;
                }
                
                if (controller.UnitOrder == null && objectivesToCapture.Count > 0)
                {
                    var objective = objectivesToCapture[index++];
                    _objectivesBeeingCaptured.Add(objective);
                    if (index >= objectivesToCapture.Count)
                        index = 0;

                    order = new CaptureOrder(unit, objective);
                } else continue;

                controller.UnitOrder = order;
                controller.UnitOrder.Execute();
            }

            _objectivesBeeingCaptured.RemoveAll(
                        x => x.GetComponent<ObjectiveController>().Stats.Owner.Equals(ObjectiveOwner.Enemy));
        }
    }
}