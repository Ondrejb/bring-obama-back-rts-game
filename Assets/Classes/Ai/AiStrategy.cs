﻿using System.Linq;
using Assets.Classes.Orders;
using Assets.Scripts;
using UnityEngine;

namespace Assets.Classes.Ai
{
    public abstract class AiStrategy
    {
        protected AiController AiController;
        protected ObjectiveManager ObjectiveManager;
        protected float NextOrdersCheck;
        private BaseController _aiBaseController;

        protected AiStrategy(AiController aiController)
        {
            AiController = aiController;
            _aiBaseController = AiController.AiBase.GetComponent<BaseController>();
            ObjectiveManager = GameObject.Find("GameController").GetComponent<ObjectiveManager>();
        }

        public abstract void Update();

        protected UnitOrder RunChecks(GameObject unit, float hitPointsRatioToFallBack, float attackRangeMultiplier)
        {
            var order = CheckDefendBase(unit);

            if (order != null)
            {
                Debug.Log("Unit moving to defend base");
                return order;
            }

            order = CheckFallBack(unit, hitPointsRatioToFallBack);

            if (order != null)
            {
                Debug.Log("Unit falling back.");
                return order;
            }

            order = CheckAssistFriendlyUnit(unit);

            if (order != null)
            {
                Debug.Log("Unit assisting friendly unit");
                return order;
            }

            order = CheckDefendObjective(unit);

            if (order != null)
            {
                Debug.Log("Unit moving to defend objective");
                return order;
            }

            order = CheckAttack(unit, attackRangeMultiplier);

            if (order != null)
            {
                Debug.Log("Unit attacking an enemy");
                return order;
            }

            // Debug.Log("No check returned a valid order.");
            return null;
        }

        protected UnitOrder CheckDefendBase(GameObject unit)
        {
            if (_aiBaseController.BaseStats.Health >= Config.MaxBaseHealth)
                return null;

            UnitOrder order = null;

            var baseHealthPerc = _aiBaseController.BaseStats.Health / (float)Config.MaxBaseHealth;
            var randNum = Random.Range(0f, 1f);

            if(randNum > baseHealthPerc)
                order = new MoveOrder(unit, AiController.AiBase.transform.position + new Vector3(0, 0, -5));

            return order;
        }

        protected UnitOrder CheckFallBack(GameObject unit, float hitPointsRatioToFallBack)
        {
            UnitOrder order = null;
            var controller = unit.GetComponent<UnitController>();

            if (controller.Stats.CurrentHealth <= controller.Stats.MaxHealth * hitPointsRatioToFallBack)
                order = new MoveOrder(unit, AiController.AiBase.transform.position);

            return order;
        }

        protected UnitOrder CheckAssistFriendlyUnit(GameObject unit)
        {
            UnitOrder order = null;

            var friendlyUnits = AiController.Units;

            foreach (var friendlyUnit in friendlyUnits)
            {
                var uc = friendlyUnit.GetComponent<UnitController>();
                var unitHealthPerc = uc.Stats.CurrentHealth/uc.Stats.MaxHealth;

                if(
                    unitHealthPerc < 0.2 || 
                    unitHealthPerc > 0.6 || 
                    Vector3.Distance(unit.transform.position, friendlyUnit.transform.position) > 25)
                    continue;

                var randNum = Random.Range(0f, 1f);

                if (randNum > unitHealthPerc)
                {
                    order = new MoveOrder(unit, friendlyUnit.transform.position);
                    break;
                }
            }

            return order;
        }

        protected UnitOrder CheckDefendObjective(GameObject unit)
        {
            var objectives = GameObject.Find("GameController").GetComponent<ObjectiveManager>().EnemyObjectives;

            return (
                from objective 
                in objectives
                let stats = objective.GetComponent<ObjectiveController>().Stats
                    where stats.Capturer.Equals(ObjectiveOwner.Player)
                    where Vector3.Distance(objective.transform.position, unit.transform.position) < (100 - stats.PerantageCaptured)*0.75
                select new MoveOrder(unit, objective.transform.position)).FirstOrDefault();
        }
        
        protected UnitOrder CheckAttack(GameObject unit, float attackRangeMultiplier)
        {
            UnitOrder order = null;

            var controller = unit.GetComponent<UnitController>();
            var attackRange = Config.GetRange(controller.Stats.AttackRange, Config.Period);
            var aggroRange = attackRange * attackRangeMultiplier;

            var playerUnits = GameObject.FindGameObjectsWithTag("PlayerUnit");
            foreach (var playerUnit in playerUnits)
            {
                var dist = Vector3.Distance(unit.transform.position, playerUnit.transform.position);
                if (dist < aggroRange && VisionSystem.HasLineOfSight(unit, playerUnit))
                {
                    order = new AttackOrder(unit, playerUnit);
                }
            }

            var pBase = GameObject.Find("PBase");
            if (pBase != null)
            {
                var pBasePos = pBase.transform.position;
                if (Vector3.Distance(pBasePos, unit.transform.position) < aggroRange)
                    order = new AttackOrder(unit, pBase);
            }

            return order;
        }
    }
}
