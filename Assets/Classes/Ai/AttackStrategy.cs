﻿using System.Collections.Generic;
using Assets.Classes.Orders;
using Assets.Scripts;
using UnityEngine;

namespace Assets.Classes.Ai
{
    public class AttackStrategy : AiStrategy
    {
        public AttackStrategy(AiController aiController) : base(aiController)
        {
        
        }

        public override void Update()
        {
            if (!(Time.time > NextOrdersCheck))
                return;

            NextOrdersCheck += AiController.NewOrdersFrequency;

            var objectivesToAttack = new List<GameObject>(ObjectiveManager.PlayerObjectives);
            objectivesToAttack.Add(GameObject.Find("PBase"));

            if (objectivesToAttack.Count == 0)
                return;

            foreach (var unit in AiController.Units)
            {
                var controller = unit.GetComponent<UnitController>();

                var order = RunChecks(unit, Config.AiFallBackHitpointsPercantage, 5f);
                if (order != null)
                {
                    controller.UnitOrder = order;
                    controller.UnitOrder.Execute();
                    continue;
                }

                if (controller.UnitOrder == null)
                {
                    var movePoint = objectivesToAttack[Random.Range(0, objectivesToAttack.Count - 1)].transform.position;
                    order = new MoveOrder(unit, movePoint);
                } else continue;

                controller.UnitOrder = order;
                controller.UnitOrder.Execute();
            }
        }
    }
}
