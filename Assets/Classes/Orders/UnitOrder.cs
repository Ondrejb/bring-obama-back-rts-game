﻿
using UnityEngine;

namespace Assets.Classes.Orders
{
    public abstract class UnitOrder
    {
        protected static float CheckOnProgressFrequency = 1;

        protected GameObject Unit;
        protected UnitController UnitController;

        protected UnitOrder(GameObject unit)
        {
            Unit = unit;
            UnitController = unit.GetComponent<UnitController>();
        }

        public abstract void Execute();

        public abstract void CheckOnProgress();

        protected void SetVelocityTowardsObjective(Vector3 movePoint)
        {
            var velocity = (movePoint - UnitController.GetComponent<Transform>().position).normalized * UnitController.Stats.Speed;
            UnitController.GetComponent<Rigidbody>().velocity = velocity;
        }
    }
}
