﻿using Assets.Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Classes.Orders
{
    public class AttackOrder : UnitOrder
    {
        private GameObject _enemyUnit;
        private Resources _resources;
        private float _attackRangeMeters;
        private float _nextDamageEvent;
        private Vector3 _lastKnownPositionOfEnemy;
        private UnitController _enemyUnitController;
        private BaseController _enemyBaseController;
        private AudioManager _audioManager;

        public AttackOrder(GameObject attackingUnit, GameObject unitOrBaseBeingAttacked) : base(attackingUnit)
        {
            _enemyUnit = unitOrBaseBeingAttacked;
            _lastKnownPositionOfEnemy = unitOrBaseBeingAttacked.transform.position;
            if (!_enemyUnit.CompareTag("Base"))
            {
                _enemyUnitController = _enemyUnit.GetComponent<UnitController>();
            }
            else
            {
                _enemyBaseController = _enemyUnit.GetComponent<BaseController>();
            }

            _resources = attackingUnit.CompareTag("PlayerUnit") 
                ? GameObject.Find("Player").GetComponent<PlayerControler>().Resources 
                : GameObject.Find("Enemy").GetComponent<AiController>().Resources;
 
            var attackRange = UnitController.Stats.AttackRange;
            _attackRangeMeters = Config.GetRange(attackRange, Config.Period);
            _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        }

        public override void Execute()
        {
            CheckOnProgress();
        }

        public override void CheckOnProgress()
        {
            if (_enemyUnit == null)
            {
                UnitController.UnitOrder = null;
                return;
            }

            if(!VisionSystem.HasLineOfSight(Unit, _enemyUnit))
            {
                if(Vector3.Distance(Unit.transform.position, _lastKnownPositionOfEnemy) > 1)
                    SetVelocityTowardsObjective(_lastKnownPositionOfEnemy);
                else
                {
                    UnitController.UnitOrder = null;
                    return;
                }
            }
            _lastKnownPositionOfEnemy = _enemyUnit.transform.position;

            if (IsInAttackRange())
            {
                if (!HasAmmo())
                {
                    // TODO give player feedback he's out of ammo?
                    return;
                }

                if (!_enemyUnit.CompareTag("Base"))
                {
                    DoDamageUnit();
                }
                else
                {
                    DoDamageBase();
                }
                _audioManager.PlayCombatSound();
                // TODO slow down instead of immediatly zero
                Unit.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
            else
                SetVelocityTowardsObjective(_enemyUnit.transform.position);
        }

        private bool HasAmmo()
        {
            return _resources.Ammunition >= UnitController.Stats.AmmoConsumptionPerAttack;
        }

        private bool IsInAttackRange()
        {
            var closestPoint = _enemyUnit.transform.position;

            if (_enemyUnit.CompareTag("Base"))
            {
                foreach (var col in _enemyUnit.GetComponents<Collider>())
                {
                    if (col.isTrigger == false)
                        closestPoint = col.ClosestPointOnBounds(Unit.transform.position);
                }
            }

            var distance = Vector3.Distance(Unit.transform.position, closestPoint);
            return distance <= _attackRangeMeters;
        }

        private void DoDamageUnit()
        {
            if (Time.time >= _nextDamageEvent)
            {
                var attackerDamage = (uint)Random.Range(UnitController.Stats.DamageMin, UnitController.Stats.DamageMax);
                var attackerAttackSpeed = UnitController.Stats.AttackSpeedMilliSecs / 1000;

                var weaponExpert = GameObject.Find("SpecialUnitWeaponExpert");

                if (weaponExpert != null && Unit.CompareTag("PlayerUnit"))
                {
                    if (Vector3.Distance(Unit.transform.position, weaponExpert.transform.position) <
                        Config.WeaponExpertRange)
                    {
                    
                        attackerDamage = (uint)(int)(attackerDamage * Config.WeaponExpertModifier);
                        
                    }
                        
                }

                Debug.Log("Damage: " + attackerDamage);

                _nextDamageEvent = Time.time + attackerAttackSpeed;
                var died = _enemyUnitController.TakeDamage(attackerDamage);
                if (died)
                    UnitController.UnitOrder = null;

                _resources.Ammunition -= UnitController.Stats.AmmoConsumptionPerAttack;
            }
        }

        private void DoDamageBase()
        {
            if (Time.time >= _nextDamageEvent)
            {
                var attackerDamage = (uint)Random.Range(UnitController.Stats.DamageMin, UnitController.Stats.DamageMax);
                var attackerAttackSpeed = UnitController.Stats.AttackSpeedMilliSecs / 1000;

                _nextDamageEvent = Time.time + attackerAttackSpeed;
                var died = _enemyBaseController.Base_TakeDamage(attackerDamage);
                if (died)
                    UnitController.UnitOrder = null;
            }
        }
    }
}
