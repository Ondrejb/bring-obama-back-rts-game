﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using UnityEngine;

namespace Assets.Classes.Orders
{
    public class CaptureOrder : UnitOrder
    {
        private GameObject _objective;
        private Vector3 _objectivePosition;
        private ObjectiveController _objectiveController;

        public CaptureOrder(GameObject unit, GameObject objective) : base(unit)
        {
            _objective = objective;
            _objectivePosition = _objective.transform.position;
            _objectiveController = _objective.GetComponent<ObjectiveController>();
        }

        public override void Execute()
        {
            CheckOnProgress();
        }

        public override void CheckOnProgress()
        {
            if (Vector3.Distance(Unit.transform.position, _objectivePosition) > 1.5)
            {
                SetVelocityTowardsObjective(_objective.transform.position);
                // Debug.Log("Moving to objective.");
            }
            else
            {
                Unit.GetComponent<Rigidbody>().velocity = Vector3.zero;

                if (Unit.CompareTag("PlayerUnit") && _objectiveController.Stats.Owner.Equals(ObjectiveOwner.Player))
                    UnitController.UnitOrder = null;

                if (Unit.CompareTag("EnemyUnit") && _objectiveController.Stats.Owner.Equals(ObjectiveOwner.Enemy))
                {
                    Debug.Log("Objective captured, moving on.");
                    UnitController.UnitOrder = null;
                }
            }
        }
    }
}
