﻿
using UnityEngine;

namespace Assets.Classes.Orders
{
    class MoveOrder : UnitOrder
    {
        private Vector3 _movePoint;
        private Rigidbody _rigidbody;
        private Transform _transform;

        private float _nextVelocityCheck;

        public MoveOrder(GameObject unit, Vector3 movePoint) : base(unit)
        {
            _movePoint = movePoint;
            _rigidbody = unit.GetComponent<Rigidbody>();
            _transform = unit.GetComponent<Transform>();
        }

        public override void Execute()
        {
            SetVelocityTowardsObjective(_movePoint);
            _nextVelocityCheck = Time.time + CheckOnProgressFrequency;
        }

        public override void CheckOnProgress()
        {
            if (Vector3.Distance(_transform.position, _movePoint) < 1)
            {
                _rigidbody.velocity = Vector3.zero;
                UnitController.UnitOrder = null;
                return;
            }

            if (Time.time >= _nextVelocityCheck)
            {
                SetVelocityTowardsObjective(_movePoint);
                _nextVelocityCheck = Time.time + CheckOnProgressFrequency;
            }
        }
    }
}
