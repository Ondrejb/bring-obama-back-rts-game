﻿
using System;

namespace Assets.Classes
{
    [Serializable]
    public class Resources
    {
        public static readonly uint ForceLimitCap = 100;

        public float RequisitionPoints;
        public float Ammunition;
        public float MedicalSupplies;
        public float ForceLimit;
    }
}
