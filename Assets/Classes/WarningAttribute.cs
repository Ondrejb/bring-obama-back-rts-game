﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.Classes
{
    [AttributeUsage(AttributeTargets.All)]
    public class WarningAttribute : System.Attribute
    {
        public readonly string Text;

        public string Topic // Topic is a named parameter
        {
            get { return topic; }
            set { topic = value; }
        }

        public WarningAttribute(string text) // Text is a positional parameter
        {
            this.Text = text;
            throw new System.ArgumentException(text);
        }

        private string topic;
    }
}