﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public enum TimeScale
    {
        Slow = 5,
        Normal = 10,
        Fast = 20
    }

    public class TimeScaleController : MonoBehaviour
    {
        public Text SpeedText;
        public TimeScale InitialScale;

        private TimeScale _timeScale;

        void Start()
        {
            _timeScale = InitialScale;
        }

        public void SlowDown()
        {
            if (_timeScale.Equals(TimeScale.Fast))
            {
                Time.timeScale = (float)TimeScale.Normal / 10;
                _timeScale = TimeScale.Normal;
                SpeedText.text = "Speed:\nNormal (1x)";
            }
            else if (_timeScale.Equals(TimeScale.Normal))
            {
                Time.timeScale = (float)TimeScale.Slow / 10;
                _timeScale = TimeScale.Slow;
                SpeedText.text = "Speed:\nSlow (0.5x)";
            }
        }

        public void SpeedUp()
        {
            if (_timeScale.Equals(TimeScale.Slow))
            {
                Time.timeScale = (float)TimeScale.Normal / 10;
                _timeScale = TimeScale.Normal;
                SpeedText.text = "Speed:\nNormal (1x)";
            }
            else if (_timeScale.Equals(TimeScale.Normal))
            {
                Time.timeScale = (float)TimeScale.Fast / 10;
                _timeScale = TimeScale.Fast;
                SpeedText.text = "Speed:\nFast (2x)";
            }
        }

    }
}