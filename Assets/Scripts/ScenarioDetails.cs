﻿using System;
using UnityEngine;

namespace Assets.Scripts
{


    [Serializable]
    public class UnitPrefabs
    {
        public GameObject Infantry1;
        public GameObject Infantry2;
        public GameObject Cavalry;
        public GameObject Artillery;

        public GameObject Scout;
        public GameObject ForwardObserver;
        public GameObject Medic;
    }

    public class ScenarioDetails : MonoBehaviour
    {
        public UnitPrefabs UnitPrefabs;

        void Start()
        {
            Config.ScenarioDetails = this;
        }
    }
}