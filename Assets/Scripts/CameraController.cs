﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float Speed;
    public float ZoomSensitivity;
    public float MinFov;
    public float MaxFov;

    public float MinPosX = -100;
    public float MaxPosX = 100;

    public float MinPosY = 0;
    public float MaxPosY = 100;

    public float MinPosZ = -100f;
    public float MaxPosZ = 100f;

    public float MinRotationY = 45.0f;
    public float MaxRotationY = 90.0f;

    public float SensX = 100.0f;
    public float SensY = 100.0f;

    private float _rotationY;
    private float _rotationX;

    void Update()
    {
        var rotationBeforeMove = transform.localEulerAngles;
        transform.localEulerAngles = new Vector3(90f, transform.localEulerAngles.y, 0.0f);

        transform.Translate(
            new Vector3(
                Input.GetAxis("Horizontal") * Speed * Time.deltaTime,
                Input.GetAxis("Vertical") * Speed * Time.deltaTime,
                0.0f));
        
        var posX= Mathf.Clamp(transform.position.x, MinPosX, MaxPosX);
        var posY = Mathf.Clamp(transform.position.y, MinPosY, MaxPosY);
        var posZ = Mathf.Clamp(transform.position.z, MinPosZ, MaxPosZ);

        transform.position = new Vector3(posX, posY, posZ);

        transform.localEulerAngles = rotationBeforeMove;

        if (Input.GetMouseButton(2))
	    {
            _rotationX += Input.GetAxis("Mouse X") * SensX * Time.deltaTime;
            _rotationY -= Input.GetAxis("Mouse Y") * SensY * Time.deltaTime;
            _rotationY = Mathf.Clamp(_rotationY, MinRotationY, MaxRotationY);
            transform.localEulerAngles = new Vector3(_rotationY, _rotationX, 0);
        }

        var fov = Camera.main.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * ZoomSensitivity;
        fov = Mathf.Clamp(fov, MinFov, MaxFov);
        Camera.main.fieldOfView = fov;
    }
}
