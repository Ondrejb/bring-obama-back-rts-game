﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class button_manager : MonoBehaviour 
{
	public void newGameButton (string NewGameLevel)
	{
		SceneManager.LoadScene (NewGameLevel);
	}

	public void QuitGame()
	{
		Application.Quit ();
	}
		
	public void PlayGame ()
	{
		if (Config.Period == Period.Roman) 
		{
			SceneManager.LoadScene ("Scenario_Roman");
		} else 
		{
			SceneManager.LoadScene ("Scenario_Modern");
		}
	}
}
 
