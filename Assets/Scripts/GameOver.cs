﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class GameOver : MonoBehaviour
    {
        public Text GameOverText;
        
        private GameObject _playerBase;
        private GameObject _enemyBase;

        private TimeManager _timeManager;

        void Start()
        {
            GameOverText.enabled = false;

            _playerBase = GameObject.Find("PBase");
            _enemyBase = GameObject.Find("EBase");
            _timeManager = GameObject.Find("GameController").GetComponent<TimeManager>();
        }

        void Update()
        {
            if(_timeManager.gameTime <= 0)
                EndGameTimeOut();

            if(_playerBase == null)
                EndGamePlayerBaseDestroyed();

            if(_enemyBase == null)
                EndGameEnemyBaseDestroyed();
        }

        private void EndGameTimeOut()
        {
//            GameOverText.enabled = true;

            var playerVp = GameObject.Find("Player").GetComponent<PlayerControler>().VictoryPoints;
            var enemyVp = GameObject.Find("Enemy").GetComponent<AiController>().VictoryPoints;

            if (playerVp > enemyVp)
//                GameOverText.text = "Time ran out. You won due to having more Victory Points.";
				SceneManager.LoadScene ("GameOverWin");
			else if (enemyVp > playerVp)
                SceneManager.LoadScene ("GameOverLoss");
//				GameOverText.text = "Time ran out. You were defeated due to having less Victory Points.";
            else
                SceneManager.LoadScene ("GameOverDraw");
//                GameOverText.text = "Time ran out. The game ended in a draw.";

            ReturnToMenu();
        }

        private void EndGamePlayerBaseDestroyed()
        {
            SceneManager.LoadScene ("GameOverLoss");
//            GameOverText.enabled = true;
//            GameOverText.text = "Game over.\nYour base has been destroyed.";

            ReturnToMenu();
        }

        private void EndGameEnemyBaseDestroyed()
        {
            SceneManager.LoadScene ("GameOverWin");
//            GameOverText.enabled = true;
//            GameOverText.text = "You won!\nThe enemy base has been destroyed.";
//
            ReturnToMenu();
        }

        private void ReturnToMenu()
        {
            Debug.Log("Returning to menu");
        }
    }
}
