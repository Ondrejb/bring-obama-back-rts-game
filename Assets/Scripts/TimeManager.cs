﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Classes.Orders;
using Assets.Scripts;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{

    public Text TimeText;
    public float gameTime ;
    private int _days;
    private int _hours;
    private int _minutes;
    private int _seconds;

    public float TimeRate = 0.25f;
    private float nextTimechange = 0.0f;
    public float TimeModifier;

    private bool _showForm;


    void Start ()
	{
        //5days
	    // gameTime = 432000;    
        TimeModifier = 240; // 1min = 4h
        TimeConverter(gameTime);
	    TimeDisplay();


	}

	
	void Update ()
	{
	    if (Time.time > nextTimechange)
	    {
	        nextTimechange = Time.time + TimeRate;
	        gameTime = gameTime  -  (0.25f*TimeModifier);
	    }

	    TimeConverter(gameTime);

	    if ((_hours == 11 || _hours == 23) && _minutes == 59)
	        _showForm = true;

	    if (_showForm && (_hours == 12 || _hours == 0) && _minutes == 0)
	    {
	        GameObject.Find("RequisitionFormController").GetComponent<RequisitionFormController>().Show();
	        _showForm = false;
	    }

        TimeDisplay();
    }

    public void TimeConverter(float currenttime)
    {
        TimeSpan t = TimeSpan.FromSeconds(currenttime);

        _seconds = t.Seconds;
        _minutes = t.Minutes;
        _hours = t.Hours;
        _days = t.Days;
        
    }

    public void TimeDisplay()
    {
        TimeText.text = string.Format("{0} Days, {1:00}:{2:00}",_days, _hours,_minutes);

    }
}
