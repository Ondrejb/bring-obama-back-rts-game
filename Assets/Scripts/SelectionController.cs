﻿using UnityEngine;
using System.Collections;

using Assets.Scripts;



public class SelectionController : MonoBehaviour {

	public void CommanderSelect (string SelectedCommander)
	{	
		
		if (SelectedCommander == "simon") {
			Config.Commander = Commander.Simon;
		
		} else if (SelectedCommander == "mariusz") {
			Config.Commander = Commander.Mariusz;

		} else if (SelectedCommander == "eivind") {
			Config.Commander = Commander.Eivind;
		}
		Debug.Log (Config.Commander);
	}
		
	public void EraSelect (string Era )
	{
		if (Era == "roman") {
			Config.Period = Period.Roman;
		}
		else if(Era == "modern")
		{
			Config.Period = Period.Modern;
		}
		Debug.Log (Config.Period);
	}
}