﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Assets.Scripts;

public enum ObjectiveType
{
    Primary = 3,
    Secondary = 2,
    Tertiary = 1
}

[Serializable]
public class ObjectiveStats
{
    public ObjectiveType Type = ObjectiveType.Tertiary;
    public ObjectiveOwner Owner = ObjectiveOwner.Neutral;
    public ObjectiveOwner Capturer;
    public int PerantageCaptured;

    public override string ToString()
    {
        if(Capturer.Equals(ObjectiveOwner.Player))
            return string.Format("Type:\t\t\t{0}\nOwner:\t\t\t{1}\nCaptured:\t\t{2}% ",
                             Type, Owner, PerantageCaptured);

        return string.Format("Type:\t\t\t{0}\nOwner:\t\t\t{1}", Type, Owner);
    }
}

public class ObjectiveController : MonoBehaviour
{
    public ObjectiveStats Stats;

    private List<GameObject> _unitsAroundObjective;
    private MeshRenderer _meshRenderer;
    private ObjectiveManager _objectiveManager;
    private float _nextPercIncrease;

    public const float CapturePercantageChangeFrequency = 2f;
    public const uint CaptureAmountPerChange = 5;

    void Start ()
	{
	    _meshRenderer = GetComponent<MeshRenderer>();
        _unitsAroundObjective = new List<GameObject>();
	    _objectiveManager = GameObject.Find("GameController").GetComponent<ObjectiveManager>();
	    _nextPercIncrease = 0;
	}

    void Update()
    {
        if (Stats.Owner.Equals(Stats.Capturer))
            return;

        if (_unitsAroundObjective.Count == 0 && Stats.Owner.Equals(ObjectiveOwner.Neutral) &&
            Stats.PerantageCaptured != 0)
        {
            if (Time.time >= _nextPercIncrease)
            {
                ChangeCapturedPerc(-5);
            }
        }

        if (!AllUnitsBelongToOneSide())
            return;

        if (Stats.PerantageCaptured < 100f && Stats.Owner.Equals(ObjectiveOwner.Neutral))
        {
            if (Time.time >= _nextPercIncrease)
            {
                ChangeCapturedPerc(5);
            }
        }
        else
        {
            if (Time.time >= _nextPercIncrease)
            {
                ChangeCapturedPerc(-5);
            }
        }

        if (Stats.PerantageCaptured == 0)
        {
            Stats.Owner = ObjectiveOwner.Neutral;
            _meshRenderer.material.color = new Color(1, 1, 1, 0.5f);
        }

        if (Stats.PerantageCaptured == 100)
        {
            Stats.Owner = Stats.Capturer;
            // Stats.Capturer ;
            _meshRenderer.material.color = Stats.Owner.Equals(ObjectiveOwner.Player) ? Color.green : Color.red;
            _objectiveManager.AddObjective(gameObject, Stats.Owner);
        }
        
    }

    private void ChangeCapturedPerc(int amount)
    {
        Stats.PerantageCaptured += amount;
        _nextPercIncrease = Time.time + CapturePercantageChangeFrequency;

        Color color;

        if (amount > 0)
        {
            color = Stats.Capturer.Equals(ObjectiveOwner.Player) ? Color.green : Color.red;
        }
        else
        {
            switch (Stats.Owner)
            {
                case ObjectiveOwner.Player:
                    color = Color.green;
                    break;
                case ObjectiveOwner.Enemy:
                    color = Color.red;
                    break;
                default:
                    color = new Color(1, 1, 1, 0.5f);
                    break;
            }
        }

        color.a = Stats.PerantageCaptured / 100f;

        if (Stats.Capturer.Equals(ObjectiveOwner.Enemy) && Stats.Owner.Equals(ObjectiveOwner.Neutral))
            color = new Color(1, 1, 1, 0.5f);

        _meshRenderer.material.color = color;
    }

    public void OnUnitEntered(GameObject unit)
    {
        if (unit.name == "SpecialUnitWeaponExpert" || unit.name == "SpecialUnitMedic")
            return;

        if (_unitsAroundObjective.Count == 0)
        {
            _nextPercIncrease = Time.time;
            var side = unit.CompareTag("PlayerUnit") ? ObjectiveOwner.Player : ObjectiveOwner.Enemy;

            if (!Stats.Capturer.Equals(side) && Stats.Owner.Equals(ObjectiveOwner.Neutral))
                Stats.PerantageCaptured = 0;

            Stats.Capturer = side;
        }

        _unitsAroundObjective.Add(unit);
    }

    public void OnUnitLeave(GameObject unit)
    {
        if (unit.name == "SpecialUnitWeaponExpert" || unit.name == "SpecialUnitMedic")
            return;

        _unitsAroundObjective.Remove(unit);

        if (!AllUnitsBelongToOneSide())
            return;

        var side = _unitsAroundObjective[0].CompareTag("PlayerUnit") ? ObjectiveOwner.Player : ObjectiveOwner.Enemy;

        if (side == Stats.Owner)
            return;

        if (!Stats.Capturer.Equals(side) && Stats.Owner.Equals(ObjectiveOwner.Neutral))
            Stats.PerantageCaptured = 0;

        Stats.Capturer = side;
    }

    private bool AllUnitsBelongToOneSide()
    {
        _unitsAroundObjective.RemoveAll(unit => unit == null);

        if (_unitsAroundObjective.Count == 0)
            return false;
        
        var unitSide = _unitsAroundObjective[0].CompareTag("PlayerUnit") ? ObjectiveOwner.Player : ObjectiveOwner.Enemy;
        
        var all = _unitsAroundObjective
            .Select(unitAroundObj => _unitsAroundObjective[0].CompareTag("PlayerUnit") ? ObjectiveOwner.Player : ObjectiveOwner.Enemy)
            .All(side => side.Equals(unitSide));

        return all;
    }
}