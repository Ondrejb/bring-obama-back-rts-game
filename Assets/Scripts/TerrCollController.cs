﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TerrCollController : MonoBehaviour
{
    private GameObject[] AllOjectives;
    private GameObject[] AllBases;

    void Start()
    {
        AllOjectives = GameObject.FindGameObjectsWithTag("Objective");
        AllBases = GameObject.FindGameObjectsWithTag("Base");

        //Terrain.activeTerrain.GetComponent<TerrainCollider>().enabled = false;

        for (int i = 0; i < AllOjectives.Length; i++)
        {
            Physics.IgnoreCollision(Terrain.activeTerrain.GetComponent<TerrainCollider>(), 
                                    AllOjectives[i].GetComponent<Collider>(), 
                                    true);
        }
        for (int i = 0; i < AllBases.Length; i++)
        {
            Physics.IgnoreCollision(Terrain.activeTerrain.GetComponent<TerrainCollider>(),
                                    AllBases[i].GetComponent<Collider>(),
                                    true);
        }

        //Terrain.activeTerrain.detailObjectDistance = 1000;
    }
}
