﻿using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip CombatSound;
    private AudioSource _combatSoundSource;

    void Start()
    {
        _combatSoundSource = gameObject.GetComponentInParent<AudioSource>();
        _combatSoundSource.clip = CombatSound;
    }

    public void PlayCombatSound()
    {
        if(_combatSoundSource.isPlaying)
            return;

        _combatSoundSource.time = Random.Range(0, CombatSound.length - 4);

        _combatSoundSource.Play();
        StartCoroutine(FadeOut(_combatSoundSource, 4f));
    }
    
    public static IEnumerator FadeOut(AudioSource audioSource, float fadeTime)
    {
        var startVolume = audioSource.volume;
        var startTime = Time.time;

        while (audioSource.volume > 0)
        {
            var elapsed = Time.time - startTime;
            audioSource.volume = startVolume - startVolume * (elapsed / fadeTime);

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }
}
