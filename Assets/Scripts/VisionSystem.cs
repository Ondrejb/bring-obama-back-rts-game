﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class VisionSystem : MonoBehaviour
{
    private List<GameObject> _playerUnits;
    private List<GameObject> _aiUnits;

    private static LayerMask _unitsAndTerrainLayers;
    private ObjectiveManager _objectiveManager;
    private GameObject _playerBase;

    void Start()
    {
        var pc = GameObject.Find("Player").GetComponent<PlayerControler>();
        _playerUnits =pc.GetComponent<PlayerControler>().Units;
        _aiUnits = GameObject.FindGameObjectWithTag("Enemy").GetComponent<AiController>().Units;
        
        _unitsAndTerrainLayers = pc.UnitsLayer + pc.TerrainLayer;
        _objectiveManager = GameObject.Find("GameController").GetComponent<ObjectiveManager>();
        _playerBase = GameObject.Find("PBase");
    }

    void Update ()
    {
        var playerObjectives = new List<GameObject>(_objectiveManager.PlayerObjectives);

        foreach (var enemyUnit in _aiUnits)
        {
            enemyUnit.GetComponent<MeshRenderer>().enabled = false;
            enemyUnit.transform.FindChild("MinimapSphere").GetComponent<MeshRenderer>().enabled = false;
            enemyUnit.transform.FindChild("Unit Highlight").GetComponent<MeshRenderer>().enabled = false;

            var unitNearBase = Vector3.Distance(enemyUnit.transform.position, _playerBase.transform.position) < 10;
            var isInLos = _playerUnits.Any(unit => HasLineOfSight(unit, enemyUnit));
            var isNearObjective =
                playerObjectives.Any(
                    objective => Vector3.Distance(enemyUnit.transform.position, objective.transform.position) < 3);

            if (!isInLos && !isNearObjective && !unitNearBase)
                continue;

            enemyUnit.GetComponent<MeshRenderer>().enabled = true;
            enemyUnit.transform.FindChild("MinimapSphere").GetComponent<MeshRenderer>().enabled = true;
            enemyUnit.transform.FindChild("Unit Highlight").GetComponent<MeshRenderer>().enabled = true;
        }
    }

    public static bool HasLineOfSight(GameObject rayFrom, GameObject rayTo)
    {
        if (rayFrom.CompareTag("PlayerUnit") && rayTo.name == "EBase")
            return true;

        RaycastHit hit;
        var dir = rayTo.transform.position - rayFrom.transform.position;
        var dist = Config.GetViewDistance(rayFrom.GetComponent<UnitController>().Stats.ViewRange);

        if (rayTo.name == "SpecialUnitScout")
            dist *= 0.5f;

        if (Physics.Raycast(rayFrom.transform.position, dir, out hit, dist, _unitsAndTerrainLayers.value))
        {
            if (hit.transform.gameObject == rayTo)
            {
                return true;
            }
        }
        return false;
    }
}