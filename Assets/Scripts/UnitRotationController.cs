﻿using UnityEngine;

public class UnitRotationController : MonoBehaviour
{
    public float sensitivity;

    void Update ()
	{
	    RaycastHit terrainHit;

	    if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z), Vector3.down, out terrainHit))
	    {
	        if (terrainHit.transform.gameObject.CompareTag("Terrain"))
	        {
                transform.rotation = Quaternion.Lerp(transform.rotation, new Quaternion(terrainHit.normal.x, terrainHit.normal.y, terrainHit.normal.z, transform.rotation.w), Time.time * sensitivity);
            }
	    }
	}
}
