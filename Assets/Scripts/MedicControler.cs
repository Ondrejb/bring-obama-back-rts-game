﻿using System;
using UnityEngine;
using System.Collections;

public class MedicControler : MonoBehaviour
{
    public uint HealAmount = 25;
    public uint BaseHealAmount = 10;

    public float HealRate = 2.0f;
    private float _nextHealing = 0.0f;

    void Start()
    {
        _nextHealing = 2.0f;
    }

    void Update()
    {

        if (Time.time > _nextHealing)
        {
            _nextHealing = Time.time + HealRate;
            ApplyHeal(gameObject.transform.position, Config.MedicRange, HealAmount, false, 0, "PlayerUnit", "");
        }
    }

    public static void ApplyHeal(Vector3 point, float radius, uint healingAmount, bool checkForMeds, uint amount, string tagToHeal, string baseName)
    {
        Collider[] hitColliders = Physics.OverlapSphere(point, radius);
        Debug.DrawLine(point, new Vector3(point.x, point.y, point.z + radius), Color.red, 100.0f);
        for (var i = 0; i <= hitColliders.Length - 1; i++)
        {

            {
                if (hitColliders[i].gameObject.CompareTag(tagToHeal))
                {
                    var currentHealth = hitColliders[i].gameObject.GetComponent<UnitController>().Stats.CurrentHealth;
                    var maxHealth = hitColliders[i].gameObject.GetComponent<UnitController>().Stats.MaxHealth;

                    if (currentHealth < maxHealth)
                    {
                        if (checkForMeds)
                        {
                            if (GameObject.Find(baseName).GetComponent<BaseController>().HasMedicalSupplies(amount))
                            {
                                if ((maxHealth - currentHealth) > healingAmount)
                                {
                                    hitColliders[i].gameObject.GetComponent<UnitController>().Stats.CurrentHealth +=
                                        healingAmount;
                                    GameObject.Find(baseName).GetComponent<BaseController>()._resources.MedicalSupplies -= Config.MedicpacksUsageBase;
                                }
                                else
                                {
                                    hitColliders[i].gameObject.GetComponent<UnitController>().Stats.CurrentHealth = maxHealth;
                                    GameObject.Find(baseName).GetComponent<BaseController>()._resources.MedicalSupplies -= Config.MedicpacksUsageBase;
                                }
                            }
                        }
                        else
                        {
                            if ((maxHealth - currentHealth) > healingAmount)
                            {
                                hitColliders[i].gameObject.GetComponent<UnitController>().Stats.CurrentHealth += healingAmount;
                            }
                            else hitColliders[i].gameObject.GetComponent<UnitController>().Stats.CurrentHealth = maxHealth;
                        }
                    }
                }
            }
        }
    }
}
