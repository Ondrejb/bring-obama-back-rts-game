﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Classes.Ai;
using UnityEngine;
using UnityEngine.UI;
using Resources = Assets.Classes.Resources;

namespace Assets.Scripts
{
    public class AiController : MonoBehaviour
    {
        public List<GameObject> Units;

        public uint VictoryPoints;
        public Text VictoryPointsText;

        public GameObject AiBase;

        public Resources Resources;

        private AiStrategy _aiStrategy;

        private PlayerControler _playerControler;
        private ObjectiveManager _objectiveManager;

        public float NewOrdersFrequency;
        public float ReevaluteStrategyFrequency;
        private float _nextReevaluation;

        public float RequisitionFrequency;
        public float FirstResourcesRequest;
        private float _nextRequisition;

        private static readonly float AmmoRatio = 0.15f;
        private static readonly float MedicalRatio = 0.1f;
        private static readonly float Infantry1Ratio = 0.30f;
        private static readonly float Infantry2Ratio = 0.20f;
        private static readonly float CavalryRatio = 0.15f;
        private static readonly float ArtilleryRatio = 0.1f;

        void Start() 
        {
            _aiStrategy = new CaptureStrategy(this);
            _playerControler = GameObject.Find("Player").GetComponent<PlayerControler>();
            _objectiveManager = GameObject.Find("GameController").GetComponent<ObjectiveManager>();
            _nextRequisition = FirstResourcesRequest + Config.ResourcesDelay;
            _nextReevaluation = ReevaluteStrategyFrequency;

            SpawnInitialUnits();
        }

        void Update() 
        {
            _aiStrategy.Update();

            if (Time.time > _nextReevaluation)
            {
                ReevaluateStrategy();
                _nextReevaluation += ReevaluteStrategyFrequency;
            }

            if (Time.time >= _nextRequisition)
            {
                MakeRequisition();
                _nextRequisition += RequisitionFrequency;
            }
        }

        private void MakeRequisition()
        {
            var costs = Config.Period == Period.Modern
                ? Config.ReqPointsCostsModern
                : Config.ReqPointsCostsRoman;

            var requestedAmmounts = new float[6];

            requestedAmmounts[0] = Mathf.FloorToInt(Resources.RequisitionPoints * AmmoRatio / costs[0]);
            requestedAmmounts[1] = Mathf.FloorToInt(Resources.RequisitionPoints * MedicalRatio / costs[1]);
            requestedAmmounts[2] = Mathf.FloorToInt(Resources.RequisitionPoints * Infantry1Ratio / costs[3]);
            requestedAmmounts[3] = Mathf.FloorToInt(Resources.RequisitionPoints * Infantry2Ratio / costs[4]);
            requestedAmmounts[4] = Mathf.FloorToInt(Resources.RequisitionPoints * CavalryRatio / costs[5]);
            requestedAmmounts[5] = Mathf.FloorToInt(Resources.RequisitionPoints * ArtilleryRatio / costs[6]);

            var forceLimit = 0;

            for (var i = 2; i < requestedAmmounts.Length; i++)
            {
                forceLimit += (int)(requestedAmmounts[i] * Config.ForceLimitCosts[i - 2]);
            }

            while (Resources.ForceLimit + forceLimit > Resources.ForceLimitCap)
            {
                Debug.Log("Overreached force limit, decreasing forces");

                for (var i = 2; i < requestedAmmounts.Length; i++)
                {
                    if (requestedAmmounts[i] > 0)
                        requestedAmmounts[i]--;

                    forceLimit -= (int)Config.ForceLimitCosts[i - 2];

                    if(Resources.ForceLimit + forceLimit <= Resources.ForceLimitCap)
                        break;
                }
            }
            Resources.ForceLimit += forceLimit;
            Resources.RequisitionPoints -= requestedAmmounts.Select((t, i) => t * costs[i]).Sum();
            Resources.Ammunition += requestedAmmounts[0];
            Resources.MedicalSupplies += requestedAmmounts[1];

            var unitsTotal = 0;
            for (var i = 2; i < requestedAmmounts.Length; i++)
                unitsTotal += (int)requestedAmmounts[i];

            var x = -2 * unitsTotal;
            var spawnPoint = AiBase.transform.position + new Vector3(x, 0, -5);

            for (var i = 0; i < requestedAmmounts[2]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry1, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }

            for (var i = 0; i < requestedAmmounts[3]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry2, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }

            for (var i = 0; i < requestedAmmounts[4]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Cavalry, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }

            for (var i = 0; i < requestedAmmounts[5]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Artillery, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }
        }
        private void SpawnInitialUnits()
        {
            var spawnPoint = GameObject.Find("EBase").transform.position + new Vector3(-4, 0, -5);
            var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry1, spawnPoint, Quaternion.identity) as GameObject;
            InitUnit(unit);

            spawnPoint += new Vector3(4, 0, 0);
            unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry1, spawnPoint, Quaternion.identity) as GameObject;
            InitUnit(unit);

            spawnPoint += new Vector3(4, 0, 0);
            unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry2, spawnPoint, Quaternion.identity) as GameObject;
            InitUnit(unit);

            Resources.ForceLimit = Config.ForceLimitCosts[0] + Config.ForceLimitCosts[0] + Config.ForceLimitCosts[1];
        }

        private void InitUnit(GameObject unit)
        {
            unit.tag = "EnemyUnit";

            var sphere = unit.transform.FindChild("MinimapSphere").gameObject;
            sphere.GetComponent<MeshRenderer>().material.color = Color.red;

            var highlight = unit.transform.FindChild("Unit Highlight").gameObject;
            highlight.GetComponent<MeshRenderer>().material.color = Color.red;

            Units.Add(unit);
        }

        public void AddVictoryPoints(uint amount)
        {
            VictoryPoints += amount;
            VictoryPointsText.text = "Enemy Victory Points: " + VictoryPoints;
        }

        private void ReevaluateStrategy()
        {
            if (ChangeToDefend())
            {
                if (_aiStrategy is DefendStrategy)
                    return;

                _aiStrategy = new DefendStrategy(this);
                Debug.Log("Ai strategy changing to Defend");
            }
            else if (ChangeToCapture())
            {
                if (_aiStrategy is CaptureStrategy)
                    return;

                _aiStrategy = new CaptureStrategy(this);
                Debug.Log("Ai strategy changing to Capture");
            }
            else if (ChangeToAttack())
            {
                if (_aiStrategy is AttackStrategy)
                    return;

                _aiStrategy = new AttackStrategy(this);
                Debug.Log("Ai strategy changing to Attack");
            }
        }

        private bool ChangeToCapture()
        {
            var hasObjectivesToCapture = _objectiveManager.NeutralObjectives.Count >= 1;
            var hasReasonableAmountOfUnits = Units.Count >= _playerControler.Units.Count * 0.5;

            return hasObjectivesToCapture && hasReasonableAmountOfUnits;
        }

        private bool ChangeToAttack()
        {
            var hasMoreOrEqualObjectives = _objectiveManager.EnemyObjectives.Count >= _objectiveManager.PlayerObjectives.Count;
            var hasMoreOrEqualUnits = Units.Count >= _playerControler.Units.Count;

            return hasMoreOrEqualObjectives && hasMoreOrEqualUnits;
        }
        private bool ChangeToDefend()
        {
            var hasConsiderablyLessUnits = Units.Count <= _playerControler.Units.Count * 0.25;

            return hasConsiderablyLessUnits;
        }
    }
}
