﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Classes.Orders;
using Assets.Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

public enum Range
{
    Short,
    Medium,
    Long,
    Special
    
}

[Serializable]
public class UnitStats
{
    public uint CurrentHealth;
    public uint MaxHealth;
    public uint DamageMin;
    public uint DamageMax;
    public float Speed;
    public float AttackSpeedMilliSecs;
    public Range AttackRange;
    public Range ViewRange;
    public uint AmmoConsumptionPerAttack;

    public override string ToString()
    {
        return string.Format("Health:\t\t\t\t{0} / {1}\nDamage:\t\t\t{2} - {3}\nSpeed:\t\t\t\t{4}\nAttack Speed:  {5}\nAttack Range:\t{6}\nView Range:\t{7}\nAmmo Cons.:\t{8} ",
            CurrentHealth, MaxHealth, DamageMin, DamageMax, Speed, AttackSpeedMilliSecs / 1000, AttackRange, ViewRange, AmmoConsumptionPerAttack);
    }
}

public class UnitController : MonoBehaviour 
{
    public UnitStats Stats;
    public string DisplayName;

    public UnitOrder UnitOrder;

    public bool isMoving;
    public bool correctYPos;

    private bool _shaking;

    void Start()
    {
        UnitOrder = null;
        correctYPos = true;
        gameObject.GetComponent<Rigidbody>().freezeRotation = true;
        _shaking = false;
    }

    void Update()
    {
        if (UnitOrder != null)
            UnitOrder.CheckOnProgress();
        var correctionY = transform.position.y;

        if (correctYPos)
        {
            // Corrects _rigidbody.y's pos to the terrains height (if over terrain)
            var terrY = Terrain.activeTerrain.SampleHeight(transform.position);
            correctionY = (terrY >= 0) ? terrY + 1f : transform.position.y;
        }

        gameObject.GetComponent<Rigidbody>().transform.position = new Vector3(transform.position.x, correctionY, transform.position.z);

        if (gameObject.GetComponent<Rigidbody>().velocity != Vector3.zero)
            isMoving = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Objective"))
        {
            var objective = other.gameObject.GetComponent<ObjectiveController>();
            objective.OnUnitEntered(gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Objective"))
        {
            var objective = other.gameObject.GetComponent<ObjectiveController>();
            objective.OnUnitLeave(gameObject);
        }
    }

    public void AddHealth(uint amount)
    {
        if (amount < Stats.MaxHealth || amount > 1) Stats.CurrentHealth += amount;
    }

    /// <summary>
    /// Damages the unit.
    /// </summary>
    /// <param name="amount">The damage</param>
    /// <returns>True, if the unit dies</returns>
    public bool TakeDamage(uint amount)
    {
        Stats.CurrentHealth -= (uint)Mathf.Clamp(amount, 0, Stats.CurrentHealth);

        if (Stats.CurrentHealth == 0)
        {
            Debug.Log("Unit died");

            if (gameObject.CompareTag("PlayerUnit"))
            {
                var pc = GameObject.Find("Player").GetComponent<PlayerControler>();
                pc.Units.Remove(gameObject);
                int forceLimitIndex;
                switch (DisplayName)
                {
                    case "Legionares":
                    case "Infantry battalion":
                        forceLimitIndex = 0;
                        break;
                    case "Archers":
                    case "Mechanized infantry":
                        forceLimitIndex = 1;
                        break;
                    case "Cavalry":
                    case "Tank battalion":
                        forceLimitIndex = 2;
                        break;
                    case "Ballistae":
                    case "Self-propelled artillery":
                        forceLimitIndex = 3;
                        break;
                    default:
                        Debug.Log("Special or uknown unit died");
                        forceLimitIndex = 4;
                        break;
                }
                pc.Resources.ForceLimit -= Config.ForceLimitCosts[forceLimitIndex];
            }
            else
            {
                var ac = GameObject.Find("Enemy").GetComponent<AiController>();
                ac.Units.Remove(gameObject);

            }

            Destroy(gameObject);

            return true;
        }

        if(!_shaking)
            StartCoroutine(ShakeAfterDamage());

        return false;
    }

    public uint GetHealth()
    {
        return Stats.CurrentHealth;
    }

    public void SetHealth(uint amount)
    {
        if (amount < Stats.MaxHealth || amount > 1) Stats.CurrentHealth = amount;
    }

    private IEnumerator ShakeAfterDamage()
    {
        _shaking = true;
        var initialRotation = transform.rotation;
        var startTime = Time.time;
        var nextChange = startTime + 0.05;
        var rigidBody = GetComponent<Rigidbody>();

        rigidBody.freezeRotation = false;
        rigidBody.angularVelocity = new Vector3(0.0f, 0.0f, 10f);

        while (Time.time <= startTime + 0.5)
        {
            if (Time.time >= nextChange)
            {
                // Change the rotation
                rigidBody.angularVelocity = rigidBody.angularVelocity *- 1;
                nextChange += 0.1;
            }

            yield return null;
        }

        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        gameObject.transform.rotation = initialRotation;
        rigidBody.freezeRotation = true;
        _shaking = false;
    }
}
