﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using Resources = Assets.Classes.Resources;

[Serializable]
public class BaseStats
{
    public uint Health;
    public uint healRate;
    public uint healAmount;
    public uint healRadius;
    public string tagToHeal;

    public override string ToString()
    {
        return string.Format("Health:\t\t\t\t{0}\nHeal Rate:\t\t\t{1} ",
            Health, healRate);
    }
}

public class BaseController : MonoBehaviour
{
    public BaseStats BaseStats;
    public List<GameObject> _enemyUnitsAroundBase;
    private float _nextHealTime;
    public Resources _resources;
    private List<GameObject> _allDeployedUnits;

    void Start()
    {
        _enemyUnitsAroundBase = new List<GameObject>();
        _allDeployedUnits = new List<GameObject>();

        _resources = gameObject.name.Equals("PBase")
                ? GameObject.Find("Player").GetComponent<PlayerControler>().Resources
                : GameObject.Find("Enemy").GetComponent<AiController>().Resources;
    }

    void LateUpdate()
    {
        if (Time.time > _nextHealTime)
        {
            MedicControler.ApplyHeal(gameObject.transform.position, BaseStats.healRadius, BaseStats.healAmount, true, 20, BaseStats.tagToHeal, gameObject.name);
            
            if (BaseStats.Health < Config.MaxBaseHealth)
            {
                if ((Config.MaxBaseHealth - BaseStats.Health) > BaseStats.healAmount)
                { BaseStats.Health += BaseStats.healAmount; }
                else
                { BaseStats.Health = Config.MaxBaseHealth; }
            }
            ApplyDamageFromBase(transform.position, 12);
            _nextHealTime = Time.time + BaseStats.healRate;
        }
    }

    public bool HasMedicalSupplies(uint amountNeeded)
    {
        return _resources.MedicalSupplies >= amountNeeded;
    }

    /// <summary>
    /// Damages the unit.
    /// </summary>
    /// <param name="amount">The damage</param>
    /// <returns>True, if the unit dies</returns>
    public bool Base_TakeDamage(uint amount)
    {
        BaseStats.Health -= (uint)Mathf.Clamp(amount, 0, BaseStats.Health);
        Debug.Log(BaseStats.Health);

        if (BaseStats.Health == 0)
        {
            Debug.Log("Base destroy");
            Destroy(gameObject);

            return true;
        }
        return false;
    }

    public void AddHealth(uint amount)
    {
        if (amount < Config.MaxBaseHealth || amount > 1) BaseStats.Health += amount;
    }

    public void TakeDamage(uint amount)
    {
        BaseStats.Health -= amount;
    }

    public uint GetHealth()
    {
        return BaseStats.Health;
    }

    public void SetHealth(uint amount)
    {
        if (amount < Config.MaxBaseHealth || amount > 1) BaseStats.Health = amount;
    }

    public static void ApplyDamageFromBase(Vector3 point, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(point, radius);
        Debug.DrawLine(point, new Vector3(point.x, point.y, point.z + radius), Color.red, 100.0f);
        List<Collider> enemyUnits = new List<Collider>();
        foreach (var unit in hitColliders)
        {
            if (unit.gameObject.CompareTag("EnemyUnit"))
            {
                enemyUnits.Add(unit);
            }
        }
        if (enemyUnits.Count > 0)
        {
            var i = UnityEngine.Random.Range(0, enemyUnits.Count);
            enemyUnits[i].gameObject.GetComponent<UnitController>().Stats.CurrentHealth -= Config.BaseDamage;
        }

    }
}
