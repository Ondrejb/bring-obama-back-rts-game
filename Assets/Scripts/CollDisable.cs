﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CollDisable : MonoBehaviour
    {
        private bool otherMoving;
        private bool myMoving;
        private static int ran;
        private GameObject lifted;
        private Vector3 liftedVel;

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("PlayerUnit") || other.gameObject.CompareTag("EnemyUnit"))
            {
                Physics.IgnoreCollision(other, gameObject.GetComponent<Collider>());
                gameObject.GetComponent<Collider>().isTrigger = true;
            }
        }

        void OnTriggerLeave(Collider other)
        {
            if (other.gameObject.CompareTag("PlayerUnit") || other.gameObject.CompareTag("EnemyUnit"))
            {
                Physics.IgnoreCollision(other, gameObject.GetComponent<Collider>());
                gameObject.GetComponent<Collider>().isTrigger = false;
                    
            }
        }
    }
}