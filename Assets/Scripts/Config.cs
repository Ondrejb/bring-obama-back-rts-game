﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;


public enum Commander
{
	Simon,
	Mariusz,
	Eivind
}

public enum Period
{
    Roman,
    Modern
}

public enum UnitType
{
    Infantry1,
    Infantry2,
    Cavalry,
    Artillery
}

public class Config : MonoBehaviour
{
    public static ScenarioDetails ScenarioDetails;
	public static Commander Commander;
	public static Period Period;


    public static readonly float AiFallBackHitpointsPercantage = 0.2f;
    
    public static float MedicRange = 10.0f;

    public static float WeaponExpertRange = 10f;
    public static float WeaponExpertModifier = 1.5f;

    public static float LongModern = 10;
    public static float MediumModern = 5;
    public static float ShortModern = 3;

    public static float LongRoman = 10;
    public static float MediumRoman = 5;
    public static float ShortRoman = 3;

    public static float SpecialViewDistance = 25.0f;
    public static float LongViewDistance = 15.0f;
    public static float MediumViewDistance = 10.0f;
    public static float ShortViewDistance = 7.5f;


    public static string Infantry1Roman = "Legionares";
    public static string Infantry2Roman = "Archers";
    public static string CavalryRoman = "Cavalry";
    public static string ArtilleryRoman = "Ballistae";

    public static string Infantry1Modern = "Mechanized infantry";
    public static string Infantry2Modern = "Infantry battalion";
    public static string CavalryModern = "Tank battalion";
    public static string ArtilleryModern = "Self-propelled artillery";

    public static float AmmoCost = 0.01f;
    public static float MedicalCost = 0.01f;
    
    public static uint Infantry1RomanCost = 20;
    public static uint Infantry2RomanCost = 20;
    public static uint CavalryRomanCost = 40;
    public static uint ArtilleryRomanCost = 40;

    public static uint Infantry1ModernCost = 20;
    public static uint Infantry2ModernCost = 20;
    public static uint CavalryModernCost = 40;
    public static uint ArtilleryModernCost = 40;

    public static uint SpecialUnitCost = 60;

    public static readonly float[] ForceLimitCosts = {12, 8, 15, 10, 5};
    public static readonly float[] ReqPointsCostsRoman = { AmmoCost, MedicalCost, Infantry1RomanCost, Infantry2RomanCost, CavalryRomanCost, ArtilleryRomanCost, SpecialUnitCost };
    public static readonly float[] ReqPointsCostsModern = { AmmoCost, MedicalCost, Infantry1ModernCost, Infantry2ModernCost, CavalryModernCost, ArtilleryModernCost, SpecialUnitCost };

    public static uint MaxBaseHealth = 10000;
    public static uint BaseDamage = 150;

    public static uint MedicpacksUsageBase = 20;

    public static float ResourcesDelay = 30;


    public static float GetRange(Range range,Period period)
    {
        switch (period)
        {
            case Period.Modern:
                switch (range)
                {
                    case Range.Short:
                        return ShortModern;
                    case Range.Medium:
                        return MediumModern;
                    case Range.Long:
                        return LongModern;
                }
                break;
            case Period.Roman:
                switch (range)
                {
                    case Range.Short:
                        return ShortRoman;
                    case Range.Medium:
                        return MediumRoman;
                    case Range.Long:
                        return LongRoman;
                }
                break;
        }
        return 0.0f;
    }

    public static string GetUnitName(UnitType unitType, Period period)
    {
        switch (period)
        {
            case Period.Modern:
                switch (unitType)
                {
                    case UnitType.Infantry1:
                        return Infantry1Modern;
                    case UnitType.Infantry2:
                        return Infantry2Modern;
                    case UnitType.Cavalry:
                        return CavalryModern;
                    case UnitType.Artillery:
                        return ArtilleryModern;
                }
                break;
            case Period.Roman:
                switch (unitType)
                {
                    case UnitType.Infantry1:
                        return Infantry1Roman;
                    case UnitType.Infantry2:
                        return Infantry2Roman;
                    case UnitType.Cavalry:
                        return CavalryRoman;
                    case UnitType.Artillery:
                        return ArtilleryRoman;
                }
                break;
        }
        return "Unknown";
    }

    public static float GetViewDistance(Range range)
    {
        float distance = 0.0f;
        switch (range)
        {
            case Range.Short:
                distance = ShortViewDistance; break;
            case Range.Medium:
                distance = MediumViewDistance; break;
            case Range.Long:
                distance = LongViewDistance; break;
            case Range.Special:
                distance = SpecialViewDistance; break;
        }
        return distance;
    }
}
