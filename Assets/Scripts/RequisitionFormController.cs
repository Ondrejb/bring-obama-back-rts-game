﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Resources = Assets.Classes.Resources;

namespace Assets.Scripts
{
    [Serializable]
    public class ReqFormUi
    {
        public GameObject Form;
        public Text PointsToSpendText;
        public Text ForceLimitText;
        public Text WarningText;
        public Text AmmunitionText;
        public Text MedicalSuppliesText;
        public Text Unit1Text;
        public Text Unit2Text;
        public Text Unit3Text;
        public Text Unit4Text;
        public Text SpecialUnitText;
        public Slider AmmunitionSlider;
        public Slider MedicalSlider;
        public Slider Unit1Slider;
        public Slider Unit2Slider;
        public Slider Unit3Slider;
        public Slider Unit4Slider;
        public Toggle SpecialUnitToggle;
        public Button SubmitButton;
    }

    public class RequisitionFormController : MonoBehaviour
    {
        public PlayerControler PlayerControler;
        
        private GameObject _playerBase;

        public ReqFormUi ReqFormUi;
        private GameObject _gameSpeedPanel;

        private float _scaleBeforeShown;
        private float _pointsToSpend;
        private float _forceLimit;

        private float[] _requestedAmmounts = {0, 0, 0, 0, 0, 0, 0};
        private float[] _reqPointsCostsPeriod;

        void Start()
        {
            _playerBase = GameObject.Find("PBase");
            _gameSpeedPanel = GameObject.Find("Game Speed");
        }

        public void Show()
        {
            _scaleBeforeShown = Time.timeScale;
            Time.timeScale = 0;

            _reqPointsCostsPeriod = Config.Period.Equals(Period.Modern) ? Config.ReqPointsCostsModern : Config.ReqPointsCostsRoman;
            ReqFormUi.Form.SetActive(true);
            _gameSpeedPanel.SetActive(false);


            UpdatePointsToSpend();
            InitTexts();
        }

        private void InitTexts()
        {
            ReqFormUi.AmmunitionText.text = "Ammunition: "+ ReqFormUi.AmmunitionSlider.value;
            ReqFormUi.MedicalSuppliesText.text = "Medical Supplies: " + ReqFormUi.MedicalSlider.value;

            ReqFormUi.Unit1Text.text = Config.GetUnitName(UnitType.Infantry1, Config.Period) + ": " + ReqFormUi.Unit1Slider.value;
            ReqFormUi.Unit2Text.text = Config.GetUnitName(UnitType.Infantry2, Config.Period) + ": " + ReqFormUi.Unit2Slider.value;
            ReqFormUi.Unit3Text.text = Config.GetUnitName(UnitType.Cavalry, Config.Period) + ": " + ReqFormUi.Unit3Slider.value;
            ReqFormUi.Unit4Text.text = Config.GetUnitName(UnitType.Artillery, Config.Period) + ": " + ReqFormUi.Unit4Slider.value;

            UpdateReqPointsAndForceLimitTexts();
        }

        public void OnSubmitClick()
        {
            StartCoroutine(SpawResources(Config.ResourcesDelay));
            PlayerControler.Resources.RequisitionPoints = _pointsToSpend;
            ReqFormUi.Form.SetActive(false);
            _gameSpeedPanel.SetActive(true);
            Time.timeScale = _scaleBeforeShown;
        }

        private void InitUnit(GameObject unit)
        {
            unit.tag = "PlayerUnit";
            var sphere = unit.transform.FindChild("MinimapSphere").gameObject;
            sphere.GetComponent<MeshRenderer>().material.color = Color.blue;

            var highlight = unit.transform.FindChild("Unit Highlight").gameObject;
            highlight.GetComponent<MeshRenderer>().material.color = Color.clear;

            PlayerControler.Units.Add(unit);
        }

        private IEnumerator SpawResources(float delaySeconds)
        {
            yield return new WaitForSeconds(delaySeconds);

            var forceLimitCost = 0;

            var unitsTotal = 0;
            for (var i = 2; i < _requestedAmmounts.Length; i++)
            {
                forceLimitCost += (int)(_requestedAmmounts[i] * Config.ForceLimitCosts[i-2]);
                unitsTotal += (int)_requestedAmmounts[i];
            }

            var resources = PlayerControler.Resources;
            resources.ForceLimit += forceLimitCost;
            resources.Ammunition += _requestedAmmounts[0];
            resources.MedicalSupplies += _requestedAmmounts[1];

            var x = -2 * unitsTotal;
            var spawnPoint = _playerBase.transform.position + new Vector3(x, 0, 5);

            for (var i = 0; i < _requestedAmmounts[2]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry1, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }

            for (var i = 0; i < _requestedAmmounts[3]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry2, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }

            for (var i = 0; i < _requestedAmmounts[4]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Cavalry, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }

            for (var i = 0; i < _requestedAmmounts[5]; i++)
            {
                var unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Artillery, spawnPoint, Quaternion.identity) as GameObject;
                InitUnit(unit);
                spawnPoint += new Vector3(4, 0, 0);
            }

            if (_requestedAmmounts[6] == 1)
            {
                GameObject specialUnit = null;

                switch (Config.Commander)
                {
                    case Commander.Mariusz:
                        specialUnit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Scout, spawnPoint, Quaternion.identity) as GameObject;
                        specialUnit.name = "SpecialUnitScout";
                        break;
                    case Commander.Simon:
                        specialUnit = Instantiate(Config.ScenarioDetails.UnitPrefabs.ForwardObserver, spawnPoint, Quaternion.identity) as GameObject;
                        specialUnit.name = "SpecialUnitWeaponExpert";
                        {
                            var specUnitRadius = specialUnit.transform.FindChild("SpecialUnitRadius").gameObject;
                            var radius = Config.WeaponExpertRange;
                            specUnitRadius.transform.localScale = new Vector3(radius, radius*20, radius);
                            specUnitRadius.GetComponent<MeshRenderer>().enabled = false;
                        }
                        break;
                    case Commander.Eivind:
                        specialUnit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Medic, spawnPoint, Quaternion.identity) as GameObject;
                        specialUnit.name = "SpecialUnitMedic";
                        {
                            var specUnitRadius = specialUnit.transform.FindChild("SpecialUnitRadius").gameObject;
                            var radius = Config.MedicRange;
                            specUnitRadius.transform.localScale = new Vector3(radius, radius*20, radius);
                            specUnitRadius.GetComponent<MeshRenderer>().enabled = false;
                        }
                        break;
                }

                InitUnit(specialUnit);
                PlayerControler.SpecialUnit = specialUnit;
            }
        }

        public void OnAmmunitionSliderChanged()
        {
            var ammount = ReqFormUi.AmmunitionSlider.value;
            ammount = (int) (ammount/100)*100;
            ReqFormUi.AmmunitionSlider.value = ammount;

            ReqFormUi.AmmunitionText.text = "Ammunition: " + ammount;
            _requestedAmmounts[0] = ammount;
            UpdatePointsToSpend();
        }
        public void OnMedicalSliderChanged()
        {
            var ammount = ReqFormUi.MedicalSlider.value;
            ammount = (int)(ammount / 100) * 100;
            ReqFormUi.MedicalSlider.value = ammount;

            ReqFormUi.MedicalSuppliesText.text = "Medical Supplies: " + ammount;
            _requestedAmmounts[1] = ammount;
            UpdatePointsToSpend();
        }

        public void OnUnit1SliderChanged()
        {
            var ammount = ReqFormUi.Unit1Slider.value;
            ReqFormUi.Unit1Text.text = Config.GetUnitName(UnitType.Infantry1, Config.Period) + ": " + ammount;
            _requestedAmmounts[2] = ammount;
            UpdatePointsToSpend();
        }
        public void OnUnit2SliderChanged()
        {
            var ammount = ReqFormUi.Unit2Slider.value;
            ReqFormUi.Unit2Text.text = Config.GetUnitName(UnitType.Infantry2, Config.Period) + ": " + ammount;
            _requestedAmmounts[3] = ammount;
            UpdatePointsToSpend();
        }
        public void OnUnit3SliderChanged()
        {
            var ammount = ReqFormUi.Unit3Slider.value;
            ReqFormUi.Unit3Text.text = Config.GetUnitName(UnitType.Cavalry, Config.Period) + ": " + ammount;
            _requestedAmmounts[4] = ammount;
            UpdatePointsToSpend();
        }
        public void OnUnit4SliderChanged()
        {
            var ammount = ReqFormUi.Unit4Slider.value;
            ReqFormUi.Unit4Text.text = Config.GetUnitName(UnitType.Artillery, Config.Period) + ": " + ammount;
            _requestedAmmounts[5] = ammount;
            UpdatePointsToSpend();
        }
        public void OnSpecialUnitToggleChanged()
        {
            var toggled = ReqFormUi.SpecialUnitToggle.isOn;
            _requestedAmmounts[6] = toggled ? 1.0f : 0;
            UpdatePointsToSpend();
        }

        private void UpdatePointsToSpend()
        {
            var reqPointsCost = _requestedAmmounts.Select((t, i) => t*_reqPointsCostsPeriod[i]).Sum();
            var forceLimitCost = Config.ForceLimitCosts.Select((t, i) => _requestedAmmounts[i + 2]*t).Sum();

            _pointsToSpend = PlayerControler.Resources.RequisitionPoints - reqPointsCost;
            _forceLimit = PlayerControler.Resources.ForceLimit + forceLimitCost;

            UpdateReqPointsAndForceLimitTexts();

            var secondSpecialUnit = PlayerControler.SpecialUnit != null && ReqFormUi.SpecialUnitToggle.isOn;

            var okToSubmit = !(_pointsToSpend < 0) && !(_forceLimit > Resources.ForceLimitCap) && !secondSpecialUnit;

            if (!okToSubmit)
            {
                ReqFormUi.WarningText.enabled = true;
                ReqFormUi.WarningText.text =
                    "Your Requisition points cannot be bellow zero and you cannot overreach your Force limit. You can also have only one special unit.";
                ReqFormUi.SubmitButton.enabled = false;
            }
            else
            {
                ReqFormUi.WarningText.enabled = false;
                ReqFormUi.WarningText.text = string.Empty;
                ReqFormUi.SubmitButton.enabled = true;
            }
        }

        private void UpdateReqPointsAndForceLimitTexts()
        {
            ReqFormUi.PointsToSpendText.text = "Points remaining: " + _pointsToSpend.ToString(CultureInfo.InvariantCulture);
            ReqFormUi.ForceLimitText.text = 
                "Force limit: " + 
                _forceLimit.ToString(CultureInfo.InvariantCulture) + 
                " / " + 
                Resources.ForceLimitCap;
        }
    }
}
