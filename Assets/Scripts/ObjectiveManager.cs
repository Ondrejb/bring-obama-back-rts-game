﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public enum ObjectiveOwner
    {
        Player,
        Enemy,
        Neutral
    }

    public class ObjectiveManager : MonoBehaviour
    {
        public PlayerControler PlayerControler;
        public AiController AiController;

        private List<GameObject> _neutralObjectives;
        public List<GameObject> NeutralObjectives
        {
            get
            {
                return new List<GameObject>(_neutralObjectives);
            }
        }

        private List<GameObject> _playerObjectives;
        public List<GameObject> PlayerObjectives
        {
            get
            {
                return new List<GameObject>(_playerObjectives);
            }
        }

        private List<GameObject> _enemyObjectives;
        public List<GameObject> EnemyObjectives
        {
            get
            {
                return new List<GameObject>(_enemyObjectives);
            }
        }

        private float _nextIncreaseTime;

        void Start()
        {
            _neutralObjectives = new List<GameObject>();
            _playerObjectives = new List<GameObject>();
            _enemyObjectives = new List<GameObject>();

            var objectives = GameObject.Find("Objectives").GetComponentsInChildren<ObjectiveController>();

            foreach (var objective in objectives)
            {
                _neutralObjectives.Add(objective.gameObject);
            }
        }

        void Update()
        {
            if (!(Time.time > _nextIncreaseTime))
                return;

            var playerPoints = CalculatePoints(_playerObjectives);
            PlayerControler.Resources.RequisitionPoints += playerPoints;

            var enemyPoints = CalculatePoints(_enemyObjectives);
            AiController.Resources.RequisitionPoints += enemyPoints;

            var playerVictoryPoints = CalculateVictoryPoints(_playerObjectives);
            PlayerControler.AddVictoryPoints(playerVictoryPoints);

            var aiVictoryPoints = CalculateVictoryPoints(_enemyObjectives);
            AiController.AddVictoryPoints(aiVictoryPoints);

            _nextIncreaseTime = Time.time + 10;
        }

        private uint CalculateVictoryPoints(ICollection<GameObject> objectives)
        {
            return (uint)objectives.Count(objective => objective.GetComponent<ObjectiveController>().Stats.Type.Equals(ObjectiveType.Primary));
        }

        public void AddObjective(GameObject objective, ObjectiveOwner owner)
        {
            if (owner.Equals(ObjectiveOwner.Player))
            {
                _playerObjectives.Add(objective);
                _enemyObjectives.Remove(objective);
                _neutralObjectives.Remove(objective);
            }
            else
            {
                _enemyObjectives.Add(objective);
                _playerObjectives.Remove(objective);
                _neutralObjectives.Remove(objective);
            }
        }

        private int CalculatePoints(ICollection<GameObject> objectivesList)
        {
            return objectivesList.Sum(point => (int)point.GetComponent<ObjectiveController>().Stats.Type) + 1; // +1 for base
        }
    }
}