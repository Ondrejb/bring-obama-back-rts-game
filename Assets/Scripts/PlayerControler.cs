﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Classes.Orders;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Resources = Assets.Classes.Resources;

namespace Assets.Scripts
{
    public class PlayerControler : MonoBehaviour
    {
        public LayerMask UnitsLayer;
        public LayerMask TerrainLayer;
        public LayerMask HoverLayer;

        public List<GameObject> Units;
        public GameObject SpecialUnit;

        public Resources Resources;
        public float RequisitionFrequency;
        public float FirstRequisitionRequest;
        private float _nextRequisitionRequest;

        public Text ReqCountText;
		public Text AmmoText;
		public Text MedPackText;
		public Text UnitCap;

        public Text SelectedUnitName;
        public Text SelectedUnitStats;

        private GameObject _selectedUnit;
        private GameObject _selectedUnitUiPanel;

        public uint VictoryPoints;
        public Text VictoryPointsText;

        public Text HoveredObjectName;
        public Text HoveredObjectStats;

        private GameObject _hoveredObject;
        private GameObject _hoveredObjectUiPanel;

        void Start()
        {
            ReqCountText.text = "RP: " + Resources.RequisitionPoints;
            _nextRequisitionRequest = FirstRequisitionRequest;

            _selectedUnit = null;

            _selectedUnitUiPanel = SelectedUnitName.gameObject.transform.parent.gameObject;
            _selectedUnitUiPanel.SetActive(false);

            _hoveredObject = null;
            _hoveredObjectUiPanel = HoveredObjectName.gameObject.transform.parent.gameObject;
            _hoveredObjectUiPanel.SetActive(false);

            SpawnInitialUnits();
        }

        void Update()
        {
            HandleInput();

            if (_selectedUnitUiPanel.activeSelf)
            {
                if(_selectedUnit == null)
                    _selectedUnitUiPanel.SetActive(false);
                else
                {
                    var unitController = _selectedUnit.GetComponent<UnitController>();
                    SelectedUnitName.text = unitController.DisplayName;
                    SelectedUnitStats.text = unitController.Stats.ToString();
                }
            }

			ReqCountText.text = "RP: " + Resources.RequisitionPoints;
			AmmoText.text = "Ammo: " + Resources.Ammunition;
			MedPackText.text = "MedKits: " + Resources.MedicalSupplies;
			UnitCap.text = "Unit Cap: " + Resources.ForceLimit + " / " + Resources.ForceLimitCap;

        }

        private void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("MainMenu");
            }

            HandleMouseHover();

            if (Input.GetMouseButton(0))
                HandleLmbClick();

            if (Input.GetMouseButton(1))
                HandleRmbClick();
        }

        private void HandleMouseHover()
        {
            _hoveredObject = GetObjectAtPos(Input.mousePosition, HoverLayer);
            if (_hoveredObject != null && _hoveredObject.CompareTag("Objective"))
            {
                HoveredObjectName.text = _hoveredObject.tag.ToString();
                HoveredObjectStats.text = _hoveredObject.GetComponent<ObjectiveController>().Stats.ToString();
                _hoveredObjectUiPanel.SetActive(true);
            }
            else if (_hoveredObject != null && _hoveredObject.CompareTag("Base"))
            {
                HoveredObjectName.text = _hoveredObject.tag.ToString();
                HoveredObjectStats.text = _hoveredObject.GetComponent<BaseController>().BaseStats.ToString();
                _hoveredObjectUiPanel.SetActive(true);
            }
            else if (_hoveredObject == null)
            {
                _hoveredObjectUiPanel.SetActive(false);
            }
        }

        private void HandleLmbClick()
        {
            var unit = GetObjectAtPos(Input.mousePosition, UnitsLayer);
            if (unit != null)
            {
                if (_selectedUnit != null && _selectedUnit.CompareTag("PlayerUnit"))
                {
                    _selectedUnit.gameObject.transform.FindChild("Unit Highlight")
                        .GetComponent<MeshRenderer>()
                        .material.color = Color.clear;

                    if (_selectedUnit.name == "SpecialUnitWeaponExpert" || _selectedUnit.name == "SpecialUnitMedic")
                    {
                        var specUnitRadius = _selectedUnit.transform.FindChild("SpecialUnitRadius").gameObject;
                        specUnitRadius.GetComponent<MeshRenderer>().enabled = false;
                    }
                }

                _selectedUnit = unit;

                if (_selectedUnit.name == "SpecialUnitWeaponExpert" || _selectedUnit.name == "SpecialUnitMedic")
                {
                    var specUnitRadius = _selectedUnit.transform.FindChild("SpecialUnitRadius").gameObject;
                    specUnitRadius.GetComponent<MeshRenderer>().enabled = true;
                }

                if (_selectedUnit.CompareTag("PlayerUnit"))
                {
                    _selectedUnit.gameObject.transform.FindChild("Unit Highlight")
                        .GetComponent<MeshRenderer>()
                        .material.color = new Color(0, 0, 1, 1);
                }
            }
            _selectedUnitUiPanel.SetActive(_selectedUnit != null);
        }

        private void HandleRmbClick()
        {
            if (_selectedUnit == null || !Units.Contains(_selectedUnit))
                return;

            var unitSelectedAudioSource = _selectedUnit.GetComponent<AudioSource>();
            unitSelectedAudioSource.Play();
            StartCoroutine(AudioManager.FadeOut(unitSelectedAudioSource, 4f));

            var unitAtPos = GetObjectAtPos(Input.mousePosition, UnitsLayer);

            if (unitAtPos != null)
            {
                if (unitAtPos.CompareTag("EnemyUnit"))
                {
                    if (_selectedUnit.GetComponent<UnitController>().UnitOrder == null)
                    {
                        _selectedUnit.GetComponent<UnitController>().UnitOrder = new AttackOrder(_selectedUnit, unitAtPos);
                        _selectedUnit.GetComponent<UnitController>().UnitOrder.Execute();
                    }
                    else
                    {
                        _selectedUnit.GetComponent<UnitController>().UnitOrder.CheckOnProgress();
                    }
                }
                else
                {
                    MoveUnitToMouseClick(_selectedUnit, MousePointToTerrainPoint(Input.mousePosition));
                }

                return;
            }

            var objectiveOrBase = GetObjectAtPos(Input.mousePosition, HoverLayer);

            if (objectiveOrBase == null || objectiveOrBase.name == "PBase" || objectiveOrBase.CompareTag("Objective"))
            {
                MoveUnitToMouseClick(_selectedUnit, MousePointToTerrainPoint(Input.mousePosition));
            }
            else if (objectiveOrBase.name == "EBase")
            {
                _selectedUnit.GetComponent<UnitController>().UnitOrder = new AttackOrder(_selectedUnit, objectiveOrBase);
                _selectedUnit.GetComponent<UnitController>().UnitOrder.Execute();
            }
        }

        public void AddPoints(int pointsToAdd)
        {
            Resources.RequisitionPoints += pointsToAdd;
            ReqCountText.text = "RP: " + Resources.RequisitionPoints;
        }

        private void MoveUnitToMouseClick(GameObject unit, Vector3 point)
        {
            var unitController = unit.GetComponent<UnitController>();
            unitController.UnitOrder = new MoveOrder(unit, point);
            unitController.UnitOrder.Execute();
        }
        
        private GameObject GetObjectAtPos(Vector3 mousePos, LayerMask layer)
        {
            Ray rayObject = Camera.main.ScreenPointToRay(mousePos);
            RaycastHit hitObjects;

            GameObject ret = null;
            if (Physics.Raycast(rayObject, out hitObjects, Mathf.Infinity, layer.value))
                ret = hitObjects.transform.gameObject;

            return ret;
        }

        private Vector3 MousePointToTerrainPoint(Vector3 mousePos)
        {
            var ray = Camera.main.ScreenPointToRay(mousePos);
            RaycastHit hit;

            if (!Physics.Raycast(ray, out hit, Mathf.Infinity, TerrainLayer.value))
                return default(Vector3);

            if (hit.transform.gameObject.CompareTag("Terrain"))
                return new Vector3(hit.point.x, Terrain.activeTerrain.SampleHeight(hit.point), hit.point.z);

            return default(Vector3);
        }
        
        public void AddVictoryPoints(uint amount)
        {
            VictoryPoints += amount;
            VictoryPointsText.text = "Player Victory Points: " + VictoryPoints;
        }
        
        private void SpawnInitialUnits()
        {
            var spawnPoint = GameObject.Find("PBase").transform.position + new Vector3(-4, 0, 5);

            GameObject unit = null;
            switch (Config.Commander)
            {
                case Commander.Mariusz:
                    unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Scout, spawnPoint, Quaternion.identity) as GameObject;
                    unit.name = "SpecialUnitScout";
                    break;
                case Commander.Simon:
                    unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.ForwardObserver, spawnPoint, Quaternion.identity) as GameObject;
                    unit.name = "SpecialUnitWeaponExpert";
                    {
                        var specUnitRadius = unit.transform.FindChild("SpecialUnitRadius").gameObject;
                        var radius = Config.WeaponExpertRange;
                        specUnitRadius.transform.localScale = new Vector3(radius, radius * 20, radius);
                        specUnitRadius.GetComponent<MeshRenderer>().enabled = false;
                    }
                    break;
                case Commander.Eivind:
                    unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Medic, spawnPoint, Quaternion.identity) as GameObject;
                    unit.name = "SpecialUnitMedic";
                    {
                        var specUnitRadius = unit.transform.FindChild("SpecialUnitRadius").gameObject;
                        var radius = Config.MedicRange;
                        specUnitRadius.transform.localScale = new Vector3(radius, radius * 20, radius);
                        specUnitRadius.GetComponent<MeshRenderer>().enabled = false;
                    }
                    break;
            }
            
            InitUnit(unit);
            SpecialUnit = unit;
            
            spawnPoint += new Vector3(4, 0, 0);
            unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry1, spawnPoint, Quaternion.identity) as GameObject;
            InitUnit(unit);

            spawnPoint += new Vector3(4, 0, 0);
            unit = Instantiate(Config.ScenarioDetails.UnitPrefabs.Infantry2, spawnPoint, Quaternion.identity) as GameObject;
            InitUnit(unit);

            Resources.ForceLimit = Config.ForceLimitCosts[0] + Config.ForceLimitCosts[1] + Config.ForceLimitCosts[4];
        }

        private void InitUnit(GameObject unit)
        {
            unit.tag = "PlayerUnit";
            var sphere = unit.transform.FindChild("MinimapSphere").gameObject;
            sphere.GetComponent<MeshRenderer>().material.color = Color.blue;

            var highlight = unit.transform.FindChild("Unit Highlight").gameObject;
            highlight.GetComponent<MeshRenderer>().material.color = Color.clear;

            Units.Add(unit);
        }
    }
}
