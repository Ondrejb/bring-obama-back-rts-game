RTS game developed as part of the NTNU's Experience design.

![Image](https://i.imgur.com/fSg4rd5.jpg)

The player’s goal is to complete a scenario by defeating the enemy. This can be achieved by securing the majority of the map and / or by destroying the opposing forces up to a point where the enemy won’t have a chance to regain control. 
The game focuses on the higher level in military command structure, so the player will not micro-manage individual soldiers or squads, but he will give orders to larger formations (companies and battalions). The player also manages resources, such as ammunition, medical equipment etc. 
There are Objectives on the map, holding of which gives the player Requisition points (RPs). These points are spent on the resources mentioned above. 

Features:

* 2 time periods (Roman and Modern)
* 4 Unit types in each period
* 3 Special units
* Smart AI